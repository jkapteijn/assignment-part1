/**
 * Created by jkapteijn on 9/11/14.
 */
// server.js

// BASE SETUP
// ==================================================w===========================

// call the packages we need
var express     = require('express'); 		// call express
    var app         = express(); 				// define our app using express
    var home        = require('./routes/home.js');     //load the route for home
    var search      = require('./routes/search.js'); //load the route for search

    var morgan      = require('morgan');


var port        = process.env.PORT || 3000; 		// set our port

// start using morgan
app.use(morgan('dev'));

app.get('/', home.index);

//app.get('/search', search.index);
app.get('/search', search.index);

//app.get('/temp', temp.index);





// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Port ' + port + ' is open to accept traffic.');