/**
 * Created by jkapteijn on 9/14/14.
 */
var https       = require("https"),
    util        = require('util'),
    url         = require('url'),
    Photo       = require('../models/photo'),
    mongoose    = require('mongoose');


    mongoose.connect('mongodb://localhost:27017/flickrCache');
    var db = mongoose.connection;


exports.index = function(req,res) {

    var searchKey = req.query.query;

    if(searchKey || ""){
        searchFlickrCache(searchKey, processResults); // check the cache first
    } else {
        returnResult({message: 'Please enter a search phrase. The expected format is: \'?query=searchphrase\'',statusCode: 404},404);
    }



    // Process Results
    // description: orchestration of results from cache versus Flickr
    // returns: calls the appropriate method to further process the data
    function processResults(err, res) {
        if (err) {
            if (res == '404') {
                console.log('no photo in cache, so go seach Flickr!');
                searchFlickr(searchKey, searchFlickrCache);

            } else if (res == '500') {
                returnResult(err, 500)
            } else {
                returnResult(err, 404)
            }
        } else {

            console.log('Cached Photo: ');
            console.log(res);
            returnResult(res, 200);
        }
    }

    // Process Photo Data
    // description: processes the data found and adds photo(s) to the cache
    // returns: JSON object of photos
    function processPhotoData(photoData) {
        var callbacks = photoData.count;
        var callbackCounter = 0;
        var photoCounter = 0;
        var cachedPhotos = [];

        if(callbacks == 1){

            addPhotoToCache(photoData.results.photo, function(err, result) {
                if (err) {
                    returnResult({message: 'Error: Something happened when adding a single photo to cache.',statusCode:'500'},500);
                }
                cachedPhotos.push(result);
                returnResult(cachedPhotos, 200);
            })


        } else {

            for (var photoItem in photoData.results.photo) {

                addPhotoToCache(photoData.results.photo[photoCounter], function (err, result) {
                    if (err) {
                        returnResult({message: 'Error: Something happened when adding multiple photos to cache.',statusCode:'500'},500);
                    }
                    cachedPhotos.push(result);
                    callbackCounter = callbackCounter + 1;

                    if (callbacks == callbackCounter) {
                        returnResult(cachedPhotos, 200);
                    }

                });
                photoCounter = photoCounter + 1;
            }
        }
    }

    // Return Results
    // description: returns result to browser using res + appropriate HTTP status code
    // returns: results to browser
    function returnResult(result, statuscode) {
        res.header('Content-Type', 'application/json;charset=utf-8');
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
        res.header('Access-Control-Allow-Credentials', false);
        res.header('Access-Control-Max-Age', '86400');
        res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');

        res.statusCode = statuscode;
        //res.send(result);

        if(result.isArray){
            res.send(result);
        } else {
            console.log('NO ARRAY');
            res.send(result);//res.send('['+result+']');
        }

    }


    // Search Flickr Cache
    // description: searches for photo in cache using the searchKey
    // returns: results from Mongo DB
    function searchFlickrCache(searchKey, callback) {

        db.on('error', function(req,res){
            console.error.bind(console, 'connection error...');
        })
        db.once('open', function callback(){
            console.log('flickrCache db opened');
        })

        Photo.find({searchString:searchKey}, function (err, photos) {
            if (err) {
                callback({message: 'Error: Something went wrong while searching for photos in cache.',statusCode:'500'},500);
            } else {
                if (photos.length >= 1) {
                    callback(null,photos);
                } else {
                    callback({message: 'Error: No photo found in cache.',statusCode:'404'},404);
                }

            }
        });
    }


    // Search Flickr
    // description: looks for photos on Flickr.com using searchKey
    // returns: photos from Cache, but only when results came from Flickr
    function searchFlickr(searchKey, callback) {

        console.log("Search for '"+searchKey+"' on Flickr...");

        var flickrUrl = 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20flickr.photos.interestingness(10)%20where%20api_key%3D%2292bd0de55a63046155c09f1a06876875%22%20and%20title%20like%20%22%25%s%25%22%20and%20ispublic%3D1%3B&format=json';

        flickrUrl = util.format(flickrUrl, searchKey);

        var flickrUrlParsed = url.parse(flickrUrl);
        var endpoint = flickrUrlParsed.path;

        var options = {
            hostname: flickrUrlParsed.hostname,
            port: 443,
            path: flickrUrlParsed.path,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }

        https.get(options, function (result) {

            var data = "";  // initialize the container for our data

            result.on("data", function (chunk) {
                data += chunk; // append this chunk to our growing `data` var
            });

            result.on("end", function () {
                //console.log(data);

                var d = JSON.parse(data);

                var photoData = d.query;

                if(photoData.count >= 1){

                    processPhotoData(photoData);

                } else {
                    returnResult({message: "Error: No photo found on Flickr for '"+searchKey+"'. Please try a different search phrase",statusCode:'404'},404);
                }

            });

            result.on("close", function () {
                console.log("CLOSE received!");
            });

            result.on("error", function(){
                console.log("Error! "+ error);
                callback('An error has occured','')
            })

        })
    };


    // Add Photo To Cache
    // description: adds photo objects to the cache
    // returns: photo object after succesfull save.
    function addPhotoToCache(p, callback){

        var photoUrl = 'https://farm'+ p.farm +'.staticflickr.com/'+ p.server +'/'+ p.id +'_'+ p.secret +'.jpg';
        var photoData = {
            'searchString':searchKey,
            'title': p.title,
            url:photoUrl
        };

        db.on('error', function(req,res){
            console.error.bind(console, 'connection error...');
        })
        db.once('open', function callback(){
            console.log('flickrCache db opened');
        })

        var cachetime = 30; //time in minutes to cache

        var photo = new Photo();
        photo.searchString = photoData.searchString;
        photo.title = photoData.title;
        photo.url = photoData.url;
        photo.creationDate = new Date().getTime();

        //photo.expireDate = new Date(photo.creationDate + cachetime * 60000).getTime();

        // save the photo and check for errors
        photo.save(function (err) {
            if (err) {
                returnResult({message: 'Error: Something went wrong while saving photo in cache.',statusCode:'500'},500);

            }
            callback(null,photo);
        });
    };


};
