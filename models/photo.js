/**
 * Created by jkapteijn on 9/12/14.
 */
var mongoose     = require('mongoose'),
    Schema       = mongoose.Schema;


var PhotoSchema   = new Schema({
    searchString:   String,
    url:            String,
    title:          String,
    creationDate:   Date,
    expireDate:     {type:Date, default:Date.now}
});

PhotoSchema.index({ expireDate: 1 }, { expireAfterSeconds: 3600 });

module.exports = mongoose.model('Photo', PhotoSchema);
//mongoose.set('debug', true)